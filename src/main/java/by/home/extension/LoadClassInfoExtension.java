package by.home.extension;

public class LoadClassInfoExtension {

    private String className = "";
    private String pathToJavaClassFile = "";
    private String message = "filepath is incorrect";

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPathToJavaClassFile() {
        return pathToJavaClassFile;
    }

    public void setPathToJavaClassFile(String pathToJavaClassFile) {
        this.pathToJavaClassFile = pathToJavaClassFile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
