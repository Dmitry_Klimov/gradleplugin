package by.home.extension;

public class pathToFileExtension {
    private String pathToFile = "";
    private String message = "filepath is incorrect";

    public String getPathToFile() {
        return pathToFile;
    }

    public void setPathToFile(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
