package by.home.tasks;

import by.home.extension.LoadClassInfoExtension;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;

public class ClassInfo extends DefaultTask {

    @TaskAction
    public void getClassFilds() throws ClassNotFoundException {
        LoadClassInfoExtension extension = getProject().getExtensions().findByType(LoadClassInfoExtension.class);
        assert extension != null;
        try {
            Class<?> entityClass = loadEntityClass(
                    extension.getPathToJavaClassFile(),
                    extension.getClassName());
            for (Field field : entityClass.getDeclaredFields()) {
                System.out.println(field.getName());
                System.out.println(field);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        //Class c = Class.forName(extension.getClassName());

    }

    private Class<?> loadEntityClass(String pathToClass, String className)
            throws MalformedURLException, ClassNotFoundException {
        URL classPathURL = Paths.get(pathToClass).toUri().toURL();
        URL[] classLoaderUrls = new URL[]{classPathURL};
        URLClassLoader urlClassLoader = new URLClassLoader(classLoaderUrls);
        return urlClassLoader.loadClass(className);
    }
}
