package by.home.tasks;

import by.home.extension.pathToFileExtension;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import java.io.BufferedReader;
import java.io.IOException;

public class FileReader extends DefaultTask {
    @TaskAction
    public void readFile() {
        pathToFileExtension extension = getProject().getExtensions().findByType(pathToFileExtension.class);

        if (extension == null)
            extension = new pathToFileExtension();

        try {
            BufferedReader reader = new BufferedReader(new java.io.FileReader(extension.getPathToFile()));

            String line;

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            setDidWork(true);

        } catch (IOException e) {
            e.printStackTrace();

            System.out.println(extension.getMessage());
            setDidWork(false);
        }
    }
}
