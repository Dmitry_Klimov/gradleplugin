package by.home;

import by.home.extension.LoadClassInfoExtension;
import by.home.extension.pathToFileExtension;
import by.home.tasks.ClassInfo;
import by.home.tasks.FileReader;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class CustomPlugin implements Plugin<Project> {
    @Override
    public void apply(Project target) {
        target.task("hello")
                .doLast(task -> System.out.println("Hello Gradle!"));
        target.getExtensions().add("dataToConnectDB", pathToFileExtension.class);
        target.getExtensions().add("classInfo", LoadClassInfoExtension.class);
        target.getTasks().create("readFile", FileReader.class);
        target.getTasks().create("getClassInfo", ClassInfo.class);
    }
}
